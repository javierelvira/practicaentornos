

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class sudokuTest.
 *
 * @author  (ALVARO LAGO, JAVIER ELVIRA, ALEJANDRO CARTES)
 * @version (28-02-19 || Fase beta V.1.0)
 */
public class sudokuTest
{
    /**
     * Default constructor for test class sudokuTest
     */
    sudoku S;
    public sudokuTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
     
    }
    
    @Test
    public void prueba1() //Sin ceros
    {
        S = new sudoku(0,5,3);
        assertEquals("No se admiten ceros",S.DimeTipo());
    }
    
    @Test
    public void prueba2() //No admite valores mayores que nueve
    {
        S = new sudoku(1,5,20);
        assertEquals("No se admiten numeros mayores que nueve",S.DimeTipo());
    }
    
    @Test
    public void prueba3() //No admite valores negativos
    {
        S = new sudoku(-2,5,3);
        assertEquals("No se admiten valores negativos",S.DimeTipo());
    }
    
    @Test
    public void prueba4() //No admiten espacios
    {
        S = new sudoku (2,' ',3);
        assertEquals("No se admiten espacios",S.DimeTipo());
    }
    
    @Test
    public void prueba5() //No admite letras
    {
        S = new sudoku (2,'A',3);
        assertEquals("No se pueden meter letras",S.DimeTipo());
    }
    
    @Test
    public void prueba6() // No admite caracteres especiales
    {
        S = new sudoku (2,'-',3);
        assertEquals("No permite caracteres especiales, solo numeros",S.DimeTipo());
    }
    /*
    @Test
    public void prueba4() //Sin ceros
    {
        S = new sudoku();
        assertEquals("No se admiten valores negativos", S.DimeTipo());
    }
    */
}
