import java.util.*;
import java.util.Random.*;
import java.awt.event.*;
import java.awt.EventQueue;

public class sudoku
{
    // instance variables - replace the example below with your own
    private int numColumnas;    //numero de columnas que tiene el juego
    private int numFilas;   //numero de filas que tiene el juego
    private int matriz[][]; //lugar en el que se almacenan los datos
    List<Integer> lista = new ArrayList<Integer>();
            int coordenadaY;
            int coordenadaX;
            int numero;
 
    /**
     * Constructor for objects of class sudoku
     */
    public sudoku(int coordenadaY, int coordenadaX, int numero) // Declaración de variables
    {
        // initialise instance variables
        numColumnas = 9;
        numFilas = 9;
        matriz = new int[numFilas][numColumnas];
        
    }
   
    public void meterNumeros()
    {   
        for (int i=0; i<300; i++) // hace que te pregunte todo el rato el numero que quieres colocar o piscar para poder terminar el sudoku
        {
        System.out.println("Indica la columna(Y)-->  ");
        Scanner X = new Scanner(System.in);
        coordenadaY = X.nextInt();

        System.out.println("Indica la fila(X)--> ");
        Scanner Y = new Scanner(System.in);
        coordenadaX = X.nextInt();

        System.out.println("Ingrese el numero: ");
        Scanner num = new Scanner(System.in);
        numero = X.nextInt();

        if ((numerovalido(coordenadaX-1, coordenadaY-1, numero))) // Lo que hace este if llama a la funcion numeravalido y comprueba que no haya numeros repetidos ni en fila, columna y cuadrante. 
        {
            System.out.println("El valor es correcto");
            matriz[coordenadaX-1][coordenadaY-1] = numero;
        }
        else
        {
            System.out.println("El valor es incorrecto");
            System.out.println("Por favor introducelo de nuevo");
        }
        TableroSudoku();
        }
    }
    
    public void rellenaCuadrante(int fila, int columna) //Esta funcion rellena un cuadrante pero hay que indicar en las variables del array
    {   
        int numero;
             inicializaLista();
             for (int cnty=fila; cnty<fila+3; cnty++)
             {
                 for (int cntx=columna; cntx<columna+3; cntx++)
                 {
                     boolean validado = false; 
                        while (!validado)
                        {
                            numero = dameNumeroLista();
                            System.out.println("probando: "+ numero);
                            if (numerovalido(cnty, cntx, numero))
                            {
                            matriz[cnty][cntx] = numero;
                            validado = true;
                            quitaLista(numero);
                            System.out.println(" "+ numero);
                            }
                        }
                 }
             }
    } 
    
    public void completaLinea(int linea){ //Completa una linea entera comprobando filas, columnas y cuadrantes.
        int numero;
        for (int cnt=0; cnt<numColumnas; cnt++)
        {
            if (matriz[linea][cnt]==0)
            {
                for (int cnt2=1; cnt2<10; cnt2++)
                {
                    if (numerovalido(linea, cnt, cnt2))
                    {
                        matriz[linea][cnt] = cnt2;
                    }
                }
            }
        }
    }

    void inicializaLista() //inicializa la lista con los 9 números
    {
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);
        lista.add(6);
        lista.add(7);
        lista.add(8);
        lista.add(9);
        System.out.println("tamaño inicial de la lista: " +lista.size());
    }
    
    int dameNumeroLista()//Coge un numero aleatorio de la lista
    {
        int pos= (int) (Math.random() * lista.size());
        System.out.println("tamaño: "+ lista.size());
        return lista.get(pos);
    }
    
    void quitaLista(int valor)//quita el valor de la lista
    {   
        for(int cnt=0; cnt<lista.size(); cnt++)
        {
            if (valor==lista.get(cnt))
            {
                lista.remove(cnt);
                return;
            }
        }
        lista.remove(valor);
    }
            
    boolean numerovalido(int fila, int columna, int valor) //Sirve para comprobar el numero que mete el usuario esta bien o no

    {
        if (ValidaFila(fila, valor))
            if (ValidaColumna(columna, valor))
                if (ValidaCuadrante(fila,columna,valor))
                    return (true);
        return(false);
    }
    
    boolean ValidaFila(int fila, int valor)  // Comprueba que cuando pongamos un numero en una fila no se vuelva a repetir en la misma fila
    {
        for(int cnt=0; cnt<numColumnas; cnt++)
        {
            if (valor==matriz[fila][cnt])
                return false;
        }
        return true;
    }
    
    boolean ValidaColumna(int columna, int valor) // Comprueba que en la columna no se repita el mismo numero
    {
        for(int cnt=0; cnt<numFilas; cnt++)
        {
            if (valor==matriz[cnt][columna])
                return false;
        }
        return true;
    }

    boolean ValidaCuadrante(int fila, int columna, int valor) //Comprueba que en el cuadrado no se repita el numero.
    {
       int ycuadrante = fila/3*3;//posicion inicial y de su cuadrante
       int xcuadrante = columna/3*3;//posicion inicial x de su cuadrante
       System.out.println("cuadrante: "+ xcuadrante+ " " + ycuadrante);
       for(int cnt=xcuadrante; cnt<(xcuadrante+3); cnt++)
       {
           for(int cnt2=ycuadrante; cnt2<(ycuadrante+3); cnt2++)
           {
               if (valor==matriz[cnt2][cnt])
               {
                    System.out.println("posicion: "+ cnt2 + " "+ cnt);
                    return (false);           
               }
           }
       }
       System.out.println("ok posicion: "+ fila + " "+ columna);
       return(true);
    }

    public void TableroSudoku(){ // Imprime el Cuadrado del Sudoku

        System.out.println("Y :\t   1   2   3    4   5   6   7   8   9  ");
        System.out.println("");
        System.out.println("X\n..");
        
        System.out.println("\t ╔════=═=═══╦══=══=════╦════=═==══╗");
        //Creamos la primera fila del sudoku
        //1,2,3 Columna de la primera fila
        System.out.print  ("1 \t ║ ");System.out.print(matriz[0][0] + " │");
        System.out.print  (" ");System.out.print(matriz[0][1] + " │");
        System.out.print  (" ");System.out.print(matriz[0][2] + " ");
        //3,4,5 Columna de la primera fila
        System.out.print  ("║ ");System.out.print(matriz[0][3] + " │");
        System.out.print  (" ");System.out.print(matriz[0][4] + " │");
        System.out.print  (" ");System.out.print(matriz[0][5] + " ");
        //7,8,9 Columna de la primera fila
        System.out.print  ("║ ");System.out.print(matriz[0][6] + " │");
        System.out.print  (" ");System.out.print(matriz[0][7] + " │");
        System.out.print  (" ");System.out.print(matriz[0][8] + " ║");
        
        //Creamos la segunda fila del sudoku
        //1,2,3 Columna de la segunda fila
        System.out.println(""); 
        System.out.print ("2\t ║ ");System.out.print(matriz[1][0] + " │");
        System.out.print  (" ");System.out.print(matriz[1][1] + " │");
        System.out.print  (" ");System.out.print(matriz[1][2] + " ");
        //3,4,5 Columna de la segunda fila
        System.out.print  ("║ ");System.out.print(matriz[1][3] + " │");
        System.out.print  (" ");System.out.print(matriz[1][4] + " │");
        System.out.print  (" ");System.out.print(matriz[1][5] + " ");
        //7,8,9 Columna de la segunda fila
        System.out.print  ("║ ");System.out.print(matriz[1][6] + " │");
        System.out.print  (" ");System.out.print(matriz[1][7] + " │");
        System.out.print  (" ");System.out.print(matriz[1][8] + " ║");
        
        //Creamos la tercera fila del sudoku
        //1,2,3 Columna de la tercera fila
        System.out.println(""); 
        System.out.print ("3\t ║ ");System.out.print(matriz[2][0] + " │");
        System.out.print  (" ");System.out.print(matriz[2][1] + " │");
        System.out.print  (" ");System.out.print(matriz[2][2] + " ");
        //3,4,5 Columna de la tercera fila
        System.out.print  ("║ ");System.out.print(matriz[2][3] + " │");
        System.out.print  (" ");System.out.print(matriz[2][4] + " │");
        System.out.print  (" ");System.out.print(matriz[2][5] + " ");
        //7,8,9 Columna de la tercera fila
        System.out.print  ("║ ");System.out.print(matriz[2][6] + " │");
        System.out.print  (" ");System.out.print(matriz[2][7] + " │");
        System.out.print  (" ");System.out.print(matriz[2][8] + " ║");
        
        System.out.println("");
        System.out.println("\t ╠═════=══=═╬═══=═=════╬══==═=════╣");
         //Creamos la cuarta fila del sudoku
        //1,2,3 Columna de la cuarta fila
        System.out.print  ("4\t ║ ");System.out.print(matriz[3][0] + " │");
        System.out.print  (" ");System.out.print(matriz[3][1] + " │");
        System.out.print  (" ");System.out.print(matriz[3][2] + " ");
        //3,4,5 Columna de la cuarta fila
        System.out.print  ("║ ");System.out.print(matriz[3][3] + " │");
        System.out.print  (" ");System.out.print(matriz[3][4] + " │");
        System.out.print  (" ");System.out.print(matriz[3][5] + " ");
        //7,8,9 Columna de la cuarta fila
        System.out.print  ("║ ");System.out.print(matriz[3][6] + " │");
        System.out.print  (" ");System.out.print(matriz[3][7] + " │");
        System.out.print  (" ");System.out.print(matriz[3][8] + " ║");
        
        //Creamos la quinta fila del sudoku
        //1,2,3 Columna de la quinta fila
        System.out.println(""); 
        System.out.print ("5\t ║ ");System.out.print(matriz[4][0] + " │");
        System.out.print  (" ");System.out.print(matriz[4][1] + " │");
        System.out.print  (" ");System.out.print(matriz[4][2] + " ");
        //3,4,5 Columna de la quinta fila
        System.out.print  ("║ ");System.out.print(matriz[4][3] + " │");
        System.out.print  (" ");System.out.print(matriz[4][4] + " │");
        System.out.print  (" ");System.out.print(matriz[4][5] + " ");
        //7,8,9 Columna de la quinta fila
        System.out.print  ("║ ");System.out.print(matriz[4][6] + " │");
        System.out.print  (" ");System.out.print(matriz[4][7] + " │");
        System.out.print  (" ");System.out.print(matriz[4][8] + " ║");
        
        //Creamos la sexta fila del sudoku
        //1,2,3 Columna de la sexta fila
        System.out.println(""); 
        System.out.print ("6\t ║ ");System.out.print(matriz[5][0] + " │");
        System.out.print  (" ");System.out.print(matriz[5][1] + " │");
        System.out.print  (" ");System.out.print(matriz[5][2] + " ");
        //3,4,5 Columna de la sexta fila
        System.out.print  ("║ ");System.out.print(matriz[5][3] + " │");
        System.out.print  (" ");System.out.print(matriz[5][4] + " │");
        System.out.print  (" ");System.out.print(matriz[5][5] + " ");
        //7,8,9 Columna de la sexta fila
        System.out.print  ("║ ");System.out.print(matriz[5][6] + " │");
        System.out.print  (" ");System.out.print(matriz[5][7] + " │");
        System.out.print  (" ");System.out.print(matriz[5][8] + " ║");       

        System.out.println("");
        System.out.println("\t ╠══=══=════╬══════==══╬═=═=═=════╣");
        
                 //Creamos la septima fila del sudoku
        //1,2,3 Columna de la septima fila
        System.out.print  ("7\t ║ ");System.out.print(matriz[6][0] + " │");
        System.out.print  (" ");System.out.print(matriz[6][1] + " │");
        System.out.print  (" ");System.out.print(matriz[6][2] + " ");
        //3,4,5 Columna de la septima fila
        System.out.print  ("║ ");System.out.print(matriz[6][3] + " │");
        System.out.print  (" ");System.out.print(matriz[6][4] + " │");
        System.out.print  (" ");System.out.print(matriz[6][5] + " ");
        //7,8,9 Columna de la septima fila
        System.out.print  ("║ ");System.out.print(matriz[6][6] + " │");
        System.out.print  (" ");System.out.print(matriz[6][7] + " │");
        System.out.print  (" ");System.out.print(matriz[6][8] + " ║");
        
        //Creamos la octava fila del sudoku
        //1,2,3 Columna de la octava fila
        System.out.println(""); 
        System.out.print ("8\t ║ ");System.out.print(matriz[7][0] + " │");
        System.out.print  (" ");System.out.print(matriz[7][1] + " │");
        System.out.print  (" ");System.out.print(matriz[7][2] + " ");
        //3,4,5 Columna de la octava fila
        System.out.print  ("║ ");System.out.print(matriz[7][3] + " │");
        System.out.print  (" ");System.out.print(matriz[7][4] + " │");
        System.out.print  (" ");System.out.print(matriz[7][5] + " ");
        //7,8,9 Columna de la octava fila
        System.out.print  ("║ ");System.out.print(matriz[7][6] + " │");
        System.out.print  (" ");System.out.print(matriz[7][7] + " │");
        System.out.print  (" ");System.out.print(matriz[7][8] + " ║");
        
        //Creamos la novena fila del sudoku
        //1,2,3 Columna de la novena fila
        System.out.println(""); 
        System.out.print ("9\t ║ ");System.out.print(matriz[8][0] + " │");
        System.out.print  (" ");System.out.print(matriz[8][1] + " │");
        System.out.print  (" ");System.out.print(matriz[8][2] + " ");
        //3,4,5 Columna de la novena fila
        System.out.print  ("║ ");System.out.print(matriz[8][3] + " │");
        System.out.print  (" ");System.out.print(matriz[8][4] + " │");
        System.out.print  (" ");System.out.print(matriz[8][5] + " ");
        //7,8,9 Columna de la novena fila
        System.out.print  ("║ ");System.out.print(matriz[8][6] + " │");
        System.out.print  (" ");System.out.print(matriz[8][7] + " │");
        System.out.print  (" ");System.out.print(matriz[8][8] + " ║");
        
        System.out.println("");
        System.out.println("\t ╚═══=══=═══╩═════==═══╩═════===══╝");

    }
    
    void llamada(){ // Genera 3 cuadrantes en las siguientes coordenadas, los que se muestran

        rellenaCuadrante(0,0);

        rellenaCuadrante(3,3);

        rellenaCuadrante(6,6);
        
        TableroSudoku();
        
        meterNumeros();
    
    }
   
    public String DimeTipo() // Son para definir los tipos de casos de test
    {
        if (coordenadaY == 0)
            return "No se admiten ceros";
        if (coordenadaX == 0)
            return "No se admiten ceros";
        if (numero == 0)
            return "No se admiten ceros";
        if (coordenadaY > 9)
            return "No se admiten numeros mayores que nueve";
        if (coordenadaX > 9)
            return "No se admiten numeros mayores que nueve";
        if (numero > 9)
            return "No se admiten numeros mayores que nueve";
        if (coordenadaY <= 0)
            return "No se admiten valores negativos";
        if (coordenadaX <= 0)
            return "No se admiten valores negativos";
        if (numero <= 0)
            return "No se admiten valores negativos";
        if (coordenadaY==(' '))
            return "No se admiten espacios";
        if (coordenadaX==(' '))
            return "No se admiten espacios";
        if (numero==(' '))
            return "No se admiten espacios";
        if (coordenadaY == 'A') // le ponemos la A como ejemplo de que no se pueden meter letras
            return "No se pueden meter letras";
        if (coordenadaX == 'A')
            return "No se pueden meter letras";
        if (numero == 'A')
            return "No se pueden meter letras";
        if (coordenadaY == '-')    // Le indicamos un - pero podria ser cualquier caracter especial
            return "No permite caracteres especiales, solo numeros";
        if (coordenadaX == '-')    
            return "No permite caracteres especiales, solo numeros";
        if (numero == '-')    
            return "No permite caracteres especiales, solo numeros";
        else
            return "No vas por un buen camino";
    }
    
    public int getA(){
        return coordenadaY;
    }
    public int getB(){
        return coordenadaX;
    }
    public int getC(){
        return numero;
    }
    public void setA(int num){
        coordenadaY=num;
    }
    public void setB(int num){
        coordenadaX=num;
    }
    public void setC(int num){
        numero=num;
    }
}

    